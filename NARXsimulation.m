clear;clc;close all;
for k=1:2 % 鑑別兩軸
%% 系統鑑別數據
if k==1
    IDdata=load('Xaxis_F1000-8000_RE2.sco');
elseif k==2
    IDdata=load('Yaxis_F1000-8000_RE2.sco');
end
inputSeries = con2seq(IDdata(:,2)');
targetSeries = con2seq(IDdata(:,3)');

%% 參數調整測試路徑
TESTorigindata=load('ID610NR1_0-100-200_RE2.sco');
m=1;
for n=1:length(TESTorigindata)
    if abs(TESTorigindata(n,2))<1e-4
        counter(m)=n;
        m=m+1;
    end
end
for n=1:length(counter)-1
    if counter(n+1)-counter(n)>1000
        s1=n;
    end
end
TESTdata=TESTorigindata(counter(s1):counter(s1+1),:);

if k==1
    Snom=TESTdata(:,2);Sact=TESTdata(:,4);
elseif k==2
    Snom=TESTdata(:,3);Sact=TESTdata(:,5);
end

%% 網路架構設定
inputDelays  = [1:3];
feedbackDelays  = [1:3];
hiddenLayerSize = 30;
narx_net = narxnet(inputDelays,feedbackDelays,hiddenLayerSize);

%% 數據前處理
% 'removeconstantrows':Process matrices by removing rows with constant values.
% 'mapminmax':Process matrices by mapping row minimum and maximum values to [-1 1].
narx_net.inputs{1}.processFcns = {'removeconstantrows','mapminmax'};
narx_net.inputs{2}.processFcns = {'removeconstantrows','mapminmax'};

%% 時間序列數據準備
[inputs,inputStates,layerStates,targets] = preparets(narx_net,inputSeries,{},targetSeries);

%% 劃分訓練數據、驗證數據、測試數據
narx_net.divideFcn = 'dividerand'; % 'dividerand':Divide targets into three sets using random indices.
narx_net.trainParam.min_grad = 1e-5;
narx_net.divideMode = 'value';  
narx_net.divideParam.trainRatio = 70/100;
narx_net.divideParam.valRatio = 15/100;
narx_net.divideParam.testRatio = 15/100;

%% 網絡訓練函數設定
narx_net.trainFcn = 'trainlm';  % Levenberg-Marquardt

%% 誤差函數設定
narx_net.performFcn = 'mse';  % Mean squared error

%% 繪圖函數設定
narx_net.plotFcns = {'plotperform','plottrainstate','plotresponse', ...
  'ploterrcorr', 'plotinerrcorr'};

%% 網絡訓練
[narx_net,tr] = train(narx_net,inputs,targets,inputStates,layerStates);

%% 網路測試
outputs = narx_net(inputs,inputStates,layerStates);
errors = gsubtract(targets,outputs);
performance = perform(narx_net,targets,outputs)

%% 計算訓練集、驗證集、測試集誤差
trainTargets = gmultiply(targets,tr.trainMask);
valTargets = gmultiply(targets,tr.valMask);
testTargets = gmultiply(targets,tr.testMask);
trainPerformance = perform(narx_net,trainTargets,outputs)
valPerformance = perform(narx_net,valTargets,outputs)
testPerformance = perform(narx_net,testTargets,outputs)

%% 網絡訓練效果可視化
figure;plotperform(tr);
figure;plottrainstate(tr);
figure;plotregression(targets,outputs);
figure;plotresponse(targets,outputs);
figure;ploterrcorr(errors);
figure;plotinerrcorr(inputs,errors);
%% 開路網路參數挑整路徑測試
gg=[Snom Sact]';
inputsXCF=con2seq(gg);
yp = sim(narx_net,inputsXCF,inputStates);
e_open = cell2mat(yp)-Sact';
figure;plot(e_open)
figure;plot(cell2mat(yp));hold on;plot(Sact)

yp2 = sim(narx_net,inputsXCF,mat2cell([0 0 0; 0 0 0],[1 1],[1 1 1]));
e_open2 = cell2mat(yp2)-Sact';
figure;plot(e_open2)
figure;plot(cell2mat(yp2));hold on;plot(Sact)

view(narx_net)
%% 閉路網路參數挑整路徑測試
narx_net_closed = closeloop(narx_net);
view(narx_net_closed)

gg2=Snom';
inputsXC=con2seq(gg2);
[p1,Pi1,Ai1,t1] = preparets(narx_net_closed,inputSeries,{},targetSeries);

yp_closed = sim(narx_net_closed,inputsXC,Pi1);
figure;plot(cell2mat(yp_closed));hold on;plot(Sact)
e_closed = cell2mat(yp_closed)-Sact';
figure;plot(e_closed)

yp_closed2 = sim(narx_net_closed,inputsXC,mat2cell([0 0 0],1,[1 1 1]));
figure;plot(cell2mat(yp_closed2));hold on;plot(Sact)
e_closed2 = cell2mat(yp_closed2)-Sact';
figure;plot(e_closed2)

save(['narx_net_closed' int2str(k) '.mat'],'narx_net_closed');
end
%% 模擬
clear;
TESTdata=load('ID610NR1_0-100-200_RE2.sco');
X_narx_net_closed=load('narx_net_closed1.mat');Y_narx_net_closed=load('narx_net_closed2.mat');
XnarxSim=sim(X_narx_net_closed.narx_net_closed,con2seq(TESTdata(:,2)'),mat2cell([0 0 0],1,[1 1 1]));
YnarxSim=sim(Y_narx_net_closed.narx_net_closed,con2seq(TESTdata(:,3)'),mat2cell([0 0 0],1,[1 1 1]));
figure;plot(TESTdata(:,2),TESTdata(:,3));hold on;axis equal;grid on;
plot(TESTdata(:,4),TESTdata(:,5));plot(cell2mat(XnarxSim),cell2mat(YnarxSim));
legend('S nom','S act','NARX sim','location','best');set(gca,'fontsize',16);
xlabel('X axis');ylabel('Y axis');
TIME=[0:0.003:(length(cell2mat(YnarxSim))-1)*0.003];
gg=[TIME' TESTdata(:,2) TESTdata(:,3) cell2mat(XnarxSim)' cell2mat(YnarxSim)'];
kk=[0 0 0 0 0];
simulations=[kk;gg];
csvwrite('NARSsimulations.csv',gg)