clear;clc;close all;

X_narx_net_closed=load('narx_net_closed1.mat');Y_narx_net_closed=load('narx_net_closed2.mat');
path_types=['C', 'D', 'K', 'S', 'SC'];
% 'C' for circular; 'D' for diamond; 'K' for KAKINO; 'S' for square; 'SC' for square with cicular conner.
C_radius=22.5;
C_scales=500;

for i=path_types   
    FileName_csv = dir([i, '*.csv']);
    FileName_sco = dir([i, '*.sco']);
    
    for j = 1 : length(FileName_csv)        
        KGM_data = csvread(FileName_csv(j).name);
        KGM_x=KGM_data(:,1);
        KGM_y=KGM_data(:,2);
        
        SERVO_data=load(FileName_sco(j).name);
         % 位置命令看機械座標，不看程式的座標，故令位置命令起點為程式起點(0, 0)。
        S_nom_x = SERVO_data(:,2)-SERVO_data(1,2);
        S_nom_y = SERVO_data(:,3)-SERVO_data(1,3);
        S_act_x = SERVO_data(:,4)-SERVO_data(1,4);
        S_act_y = SERVO_data(:,5)-SERVO_data(1,5);
        
        XnarxSim=sim(X_narx_net_closed.narx_net_closed,con2seq(S_nom_x'),mat2cell([0 0 0],1,[1 1 1]));
        YnarxSim=sim(Y_narx_net_closed.narx_net_closed,con2seq(S_nom_y'),mat2cell([0 0 0],1,[1 1 1]));
        
        if i == 'C'
            r_act=sqrt(S_act_x.^2+(S_act_y-C_radius).^2);
            contour_error_act_x = (r_act-C_radius).*S_act_x./r_act;
            contour_error_act_y = (r_act-C_radius).*(S_act_y-C_radius)./r_act;
            
            r_KGM=sqrt(KGM_x.^2+(KGM_y-C_radius).^2);
            contour_error_KGM_x = (r_KGM-C_radius).*KGM_x./r_KGM;
            contour_error_KGM_y = (r_KGM-C_radius).*(KGM_y-C_radius)./r_KGM;
            
            r_SIM=sqrt(cell2mat(XnarxSim).^2+(cell2mat(YnarxSim)-C_radius).^2);            
            contour_error_SIM_x = (r_SIM-C_radius).*cell2mat(XnarxSim)./r_SIM;
            contour_error_SIM_y = (r_SIM-C_radius).*(cell2mat(YnarxSim)-C_radius)./r_SIM;

        end       
        
        figure(); hold on; axis equal; grid on; 
        if i == 'C'
            title(['Contour error of ' FileName_csv(j).name]);
            plot(S_nom_x, S_nom_y,'k','LineWidth',2);
            plot(S_act_x+C_scales.*contour_error_act_x, S_act_y+C_scales.*contour_error_act_y,'cyan','LineWidth',2);
            plot(KGM_x+C_scales.*contour_error_KGM_x, KGM_y+C_scales.*contour_error_KGM_y,'g','LineWidth',2);
            plot(cell2mat(XnarxSim)+C_scales.*contour_error_SIM_x, cell2mat(YnarxSim)+C_scales.*contour_error_SIM_y,'r','LineWidth',2);
            legend('S nom','S act(x500)', 'KGM(x500)','NARX(x500)','location','best');
        else
            title(['Path of ' FileName_csv(j).name]);
            plot(S_nom_x, S_nom_y,'-.','LineWidth',2);
            plot(S_act_x, S_act_y,'-.','LineWidth',2);
            plot(cell2mat(XnarxSim),cell2mat(YnarxSim),'-.','LineWidth',2);
            plot(KGM_x, KGM_y,'-.','LineWidth',2);
            legend('S nom','S act','NARX','KGM','location','best');
        end
        xlabel('X axis');ylabel('Y axis');set(gca,'fontsize',16);
        
        
    end
    
end